﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Groporte.Web.Models;
using Groporte.Core.Interfaces.Providers;

namespace Groporte.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUserContext _userContext;

        public HomeController(IUserContext userContext)
        {
            _userContext = userContext;
        }

        public IActionResult Index()
        {
            var claims = HttpContext.User.Claims;

            var id = Guid.Parse("5b2d8e5a-e05b-4b11-a45d-1448d8d4813f");

            var adminStatus = _userContext.IsUserIdAdminAsync(id).GetAwaiter().GetResult();

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
