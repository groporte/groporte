﻿using Groporte.Core.Constants;
using Groporte.Core.Helpers;
using Groporte.Core.Interfaces.Providers;
using Groporte.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Groporte.Web.Providers
{
    public class UserContext : IUserContext
    {
        private readonly HttpContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        //private readonly ApplicationDbContext _applicationDbContext;

        /// <summary>
        /// constructor for UserContext
        /// </summary>
        /// <param name="context"></param>
        public UserContext(
            IHttpContextAccessor context,
            UserManager<ApplicationUser> userManager)
        {
            _context = context.HttpContext;
            _userManager = userManager;
        }

        /// <summary>
        /// Gets email for the logged in user
        /// </summary>
        /// <returns></returns>
        public string GetUserEmail()
        {
            var email = _context.User.Claims.SingleOrDefault(c => c.Type == ClaimTypes.Name)?.Value;

            if (email.IsNullOrEmpty())
            {
                throw new Exception("Email not found for user. Perhaps user is not currently logged in.");
            }

            return email;
        }

        /// <summary>
        /// Gets role for the logged in user
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetUserRoles()
        {
            return _context.User.Claims
                .Where(c => c.Type == ClaimTypes.Role)
                .Select(x => x.Value);
        }

        public bool IsAdmin()
        {
            var roles = GetUserRoles();

            return roles.Contains(RolesConstant.Agent);
        }

        public async Task<bool> IsUserIdAdminAsync(Guid id)
        {
            //var stringId = id.ToString();

            //var user = await _userManager.Users.FirstOrDefaultAsync(x => x.Id == stringId);

            var user = new ApplicationUser { Id = id };

            var roles = await _userManager.GetRolesAsync(user);

            return roles.Contains(RolesConstant.Agent);
        }

        public Guid UserId()
        {
            return Guid.Parse(_context.User.Claims.SingleOrDefault(c => c.Type == ClaimTypes.NameIdentifier)?.Value);
        }
    }
}
