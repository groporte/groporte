﻿var app = app || {};

app.product = app.product || {};

(function (vm, modalDialog, common) {
    'use strict';

    vm.init = function () {
        $('#product #create-product').click(handleAddClick);
        $('#product .view-product').click(handleViewClick);
        $('#product .edit-product').click(handleUpdateClick);
        $('#product .delete-product').click(handleDeleteClick);
    };

    function handleAddClick(e) {
        e.preventDefault();

        $.get('/dashboard/product/create', function (htmlContent) {
            var modal = new modalDialog();

            var buttons = [
                {
                    label: 'Create',
                    btnClass: 'btn-primary',
                    action: function () {
                        var formData = app.common.serializeForm($('form#createProductForm'));

                        $.post('/dashboard/product/create', formData, function (data) {
                            location.href = '/dashboard/product';
                        });
                    }
                }
            ];

            modal.show(htmlContent, 'Create Product', buttons, 'lg');
        });
    }

    function handleViewClick(e) {
        e.preventDefault();

        var productId = getProductId(e);

        $.get('/dashboard/product/details/' + productId, function (htmlContent) {
            var modal = new modalDialog();

            var buttons = [
                {
                    label: 'Delete',
                    btnClass: 'btn-primary',
                    action: function () {
                        common.confirmDialog.show('Delete Product', 'Delete').done(function () {
                            $.post('/dashboard/product/delete/' + productId).done(function () {
                                toastr.success('Product deleted');

                                location.href = '/dashboard/product';
                            });
                        });
                    }
                }
            ];

            modal.show(htmlContent, 'Product Details', buttons, 'lg');
        });
    }

    function handleUpdateClick(e) {
        e.preventDefault();

        var productId = getProductId(e);

        $.get('/dashboard/product/edit/' + productId).done(function (htmlContent) {
            var modal = new modalDialog();

            var buttons = [
                {
                    label: 'Update',
                    btnClass: 'btn-primary',
                    type: 'submit',
                    action: function (e) {
                        e.preventDefault();

                        var formIsValid = app.common.valid('form#editProductForm');

                        if (!formIsValid) return;

                        var formData = app.common.serializeForm($('form#editProductForm'));

                        $.post('/dashboard/product/edit/' + productId, formData).done(function (data) {
                            toastr.success('Product Updated');

                            location.href = '/dashboard/product';
                        });
                    }
                }
            ];

            modal.show(htmlContent, 'Update Product', buttons, 'lg');
        });
    }

    function handleDeleteClick(e) {
        e.preventDefault();

        var productId = getProductId(e);

        common.confirmDialog.show('<section class="container">Are you sure you want to delete</section>', 'Delete').done(function () {
            $.post('/dashboard/product/delete/' + productId).done(function () {
                toastr.success('Product deleted');

                location.href = '/dashboard/product';
            });
        });
    }

    function getProductId(e) {
        return $(e.currentTarget).closest('tr').data('productid');
    }

}(app.product, app.ModalDialog, app.common));
