﻿var app = app || {};

app.order = app.order || {};

(function (vm, modalDialog, common) {
    'use strict';
    var orders = [];

    var orderStatus = {
        none: 0,
        pending: 100,
        inProgress: 200,
        completed: 300,
        cancelled: 400
    };

    vm.init = function (_orders) {
        orders = _orders;

        $('#order #create-order').click(handleAddClick);
        $('#order .view-order').click(handleViewOrderClick);
        $('#order .edit-order').click(handleUpdateClick);
        $('#order .delete-order').click(handleCancelClick);
    };

    function handleAddClick(e) {
        e.preventDefault();

        $.get('/dashboard/order/create').done(function (htmlContent) {
            var modal = new modalDialog();

            var buttons = [
                {
                    label: 'Create',
                    btnClass: 'btn-primary',
                    type: 'submit',
                    action: function (e) {
                        e.preventDefault();

                        var formIsValid = common.valid('form#createOrderForm');

                        if (!formIsValid) return;

                        var formData = common.serializeForm($('form#createOrderForm'));

                        $.post('/dashboard/order/create', formData).done(function (data) {
                            toastr.success('Order Created');

                            location.href = '/dashboard/order';
                        });
                    }
                }
            ];

            modal.show(htmlContent, 'Create Order', buttons, 'lg');
        });
    }

    function handleViewOrderClick(e) {
        e.preventDefault();

        var orderId = getOrderId(e);

        $.get('/dashboard/order/details/' + orderId, function (htmlContent) {
            var modal = new modalDialog();

            var buttons = [];

            var order = orders.find(function (order) { return order.id === orderId; });

            if (order.orderStatus === orderStatus.pending) {
                buttons = [
                    {
                        label: 'Pay Now',
                        btnClass: 'btn-primary',
                        action: function () {
                            $.post('/dashboard/order/pay', { id: orderId }, function (data) {
                                toastr.success('Payment Successful');

                                location.href = '/dashboard/order';
                            });
                        }
                    }
                ];
            }

            modal.show(htmlContent, 'Order Details', buttons, 'lg');
        });
    }

    function handleUpdateClick(e) {
        e.preventDefault();

        var orderId = getOrderId(e);

        $.get('/dashboard/order/edit/' + orderId).done(function (htmlContent) {
            var modal = new modalDialog();

            var buttons = [
                {
                    label: 'Update',
                    btnClass: 'btn-primary',
                    type: 'submit',
                    action: function (e) {
                        e.preventDefault();

                        var formIsValid = app.common.valid('form#editOrderForm');

                        if (!formIsValid) return;

                        var formData = app.common.serializeForm($('form#editOrderForm'));

                        $.post('/dashboard/order/edit/' + orderId, formData).done(function (data) {
                            toastr.success('Order Created');

                            location.href = '/dashboard/order';
                        });
                    }
                }
            ];

            modal.show(htmlContent, 'Update Order', buttons, 'lg');
        });
    }

    function handleCancelClick(e) {
        e.preventDefault();

        var orderId = getOrderId(e);

        $.get('/dashboard/order/cancel/' + orderId).done(function (htmlContent) {
            var modal = new modalDialog();

            var buttons = [
                {
                    label: 'No I don\'t want to',
                    btnClass: 'btn-primary',
                    action: function (e, modal) {
                        e.preventDefault();

                        modal.hide();
                    }
                },
                {
                    label: 'Cancel Order',
                    btnClass: 'btn-lg btn-danger',
                    type: 'submit',
                    action: function (e) {
                        e.preventDefault();

                        $.post('/dashboard/order/cancel/' + orderId).done(function (data) {
                            toastr.success('Order Cancelled');

                            location.href = '/dashboard/order';
                        });
                    }
                }
            ];

            modal.show(htmlContent, 'Cancel Order', buttons, 'lg');
        });
    }

    function getOrderId(e) {
        return $(e.currentTarget).closest('tr').data('orderid');
    }

}(app.order, app.ModalDialog, app.common));
