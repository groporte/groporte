var app = app || {};

app.common = app.common || {};

(function (vm) {
    'use strict';

    vm.initPage = function () {
        $("#menu-toggle").click(function (e) {
            e.preventDefault();

            $("#wrapper").toggleClass("toggled");
        });

        vm.initAjaxSetup();
        vm.initConfirmButtons();
        vm.initExecuteButtons();
    };

    vm.initDatatable = function () {
        $('table').DataTable({
            pageLength: 50
        });
    };

    vm.initAjaxSetup = function () {
        //init spinner for during ajax calls 
        var $loadingSpinner = $('#loading-spinner');

        $(document).ajaxStart(function () {
            $loadingSpinner.show();
        });

        $(document).ajaxStop(function () {
            $loadingSpinner.hide();
        });

        $(document).ajaxError(function (event, jqxhr) {
            $loadingSpinner.hide();

            toastr.error(jqxhr.responseText);
        });

        // disable caching for ajax calls
        $.ajaxSetup({
            cache: false
        });
    };

    vm.initExecuteButtons = function () {
        $('body').on('click', '[data-execute=true]', function (e) {
            e.preventDefault();
            var $element = $(this);
            var elementData = $element.data();
            var url = elementData.url;
            var redirect = elementData.redirect;
            var onExecute = elementData.onexecute;
            var requestData = elementData.data;
            var message = elementData.confirmmessage;
            var title = elementData.confirmtitle || 'Confirm';

            vm.runExecute(url, redirect, onExecute, requestData, message, title);
        });
    };

    vm.runExecute = function (url, redirect, onExecute, requestData, confirmMessage, confirmMessageTitle) {
        vm.confirmDialog.show(confirmMessage, confirmMessageTitle).done(function () {
            $.post(url, requestData)
                .done(function (result) {
                    if (redirect) {
                        location.href = redirect.replace('{result}', result);
                    }

                    if (onExecute) {
                        eval(onExecute);
                    }
                })
                .fail(function (err) {
                    toastr.error(err.statusText, 'Failed!');
                });
        });
    };

    vm.initConfirmButtons = function () {
        $('body').on('click', '[data-confirm=true]', function (e) {
            e.preventDefault();
            var $element = $(this);
            var data = $element.data();
            var message = data.confirmmessage;
            var title = data.confirmtitle || 'Confirm';

            vm.confirmDialog.show(message, title).done(function () {
                $element.parents('form').submit();
            });
        });
    };

    vm.confirmDialog = new SimpleDialog(true, true);

    vm.okDialog = new SimpleDialog(true, false);

    vm.getFormData = function (form) {
        var formData = {};
        var $form = typeof form === 'string' ? $(form) : form;

        $form.find('input:not([type=submit],[type=button]), select, textarea')
            .each(function (index, element) {
                if (!element.name)
                    return;

                else if (element.tagName === 'SELECT' && element.type === 'select-multiple') {
                    formData[element.name] = $('#' + element.id + ' :selected').toArray().map(function (input) {
                        return input.value;
                    });

                    return;
                }

                formData[element.name] = element.value;
            });

        return formData;
    };

    vm.serializeForm = function (form, useTraditional) {
        var formData = vm.getFormData(form);

        return $.param(formData, !!useTraditional);
    };

    vm.initFormValidation = function ($form) {
        $form.removeData('validator');
        $form.removeData('unobtrusiveValidation');

        $.validator.unobtrusive.parse($form);
    };

    vm.valid = function (target) {
        return $(target).valid();
    };

    vm.initFormValidation = function ($form) {
        $form.removeData('validator');
        $form.removeData('unobtrusiveValidation');

        $.validator.unobtrusive.parse($form);
    };

    vm.getHashQueryStringParameter = function (name) {
        var hashQueryStringParameters = window.location.hash.slice(1).split('&');

        var queryStringKeyPair = hashQueryStringParameters
            .map(function (p) {
                try {
                    return {
                        name: decodeURIComponent(p.split("=")[0]),
                        value: decodeURIComponent(p.split("=")[1])
                    };
                } catch (e) {
                    console.error(e);
                }

                return null;
            })
            .find(function (p) {
                return p !== null && p.name.toLowerCase() === name.toLowerCase();
            });

        return queryStringKeyPair && queryStringKeyPair.value;
    };

    function SimpleDialog(hasOkBtn, hasCancelBtn) {
        var modal = new app.ModalDialog();
        var buttons = [];

        if (hasCancelBtn) {
            buttons.push(modal.cancelButton());
        }

        if (hasOkBtn) {
            buttons.push(modal.okButton());
        }

        function showDialog(message, title, size) {
            var dialogSize = size || 'lg';

            return modal.show(message, title, buttons, dialogSize);
        }

        return {
            show: showDialog,
            hide: modal.hide
        };
    }
}(app.common));
