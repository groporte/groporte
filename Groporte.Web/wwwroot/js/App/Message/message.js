﻿var app = app || {};

app.message = app.message || {};

(function (vm, common) {
    'use strict';

    var connection;

    vm.init = function () {
        vm.processHash();

        $('button.msg_send_btn').click(sendMessage);

        $('div.inbox-list div.chat').click(getOrderChatHistory);

        connection = new signalR.HubConnectionBuilder().withUrl('/messageHub').build();

        //Disable send button until connection is established
        $('button.msg_send_btn').attr('disabled', true);

        connection.on('ReceiveMessage', function (conversation) {
            $.post('/Dashboard/Message/IsOutgoing', { senderId: conversation.senderId }).then(function (isOutgoing) {
                conversation.isOutgoing = isOutgoing;

                $('div.input_msg_write .write_msg').val('');

                appendHtmlChat(conversation);
            });
        });

        connection.start().then(function () {
            $('button.msg_send_btn').removeAttr('disabled');
        }).catch(function (err) {
            return console.error(err.toString());
        });
    };

    vm.processHash = function () {
        var orderId = common.getHashQueryStringParameter('orderId');

        $('.msg_history').load('/Dashboard/Message/GetOrderMessageHistoryPartial', $.param({ orderId: orderId }), function (data) {
            $('div.inbox-list div.chat').removeClass('active-chat');

            if (orderId) {
                $('div.inbox-list div.chat[data-orderid="' + orderId + '"]').addClass('active-chat');
            }
            else {
                $('div.inbox-list div.chat:first').addClass('active-chat');
            }

            $('.msg_history').animate({ scrollTop: 20000000 }, "slow");
        });
    };

    function sendMessage(e) {
        var typedMessage = $('div.input_msg_write .write_msg').val().trim();
        
        if (!typedMessage) return;

        var data = {
            orderId: $('.active-chat').data('orderid'),
            message: typedMessage
        };

        connection.invoke('SendMessageAsync', data).catch(function (err) {
            return console.error(err.toString());
        });
    }

    function getOrderChatHistory(e) {
        var $currentTarget = $(e.currentTarget);

        var orderId = $currentTarget.data('orderid');

        $('.msg_history').load('/Dashboard/Message/GetOrderMessageHistoryPartial', $.param({ orderId: orderId }), function () {
            location.hash = 'orderid=' + orderId;

            $('div.inbox-list div.chat').removeClass('active-chat');

            $currentTarget.addClass('active-chat');

            $('.msg_history').animate({ scrollTop: 20000000 }, "slow");
        });
    }

    function appendHtmlChat(conversation) {
        var $msgContainerDiv = $('<div></div>');
        var $msgDiv = $('<div></div>');

        if (conversation.isOutgoing) {
            $msgContainerDiv.addClass('outgoing_msg');
            $msgDiv.addClass('sent_msg');

            $msgDiv.append(`<p>${conversation.message}</p>
                        <span class="time_date"> ${conversation.createdDate}</span>`);
        }
        else {
            $msgContainerDiv.addClass('incoming_msg');

            $msgDiv.addClass('received_msg');

            $msgDiv.append($('<div class="received_withd_msg"></div>').append(`<p>${conversation.message}</p>
                        <span class="time_date"> ${conversation.createdDate}</span>`));
        }

        $msgContainerDiv.append($msgDiv);

        $('.msg_history').append($msgContainerDiv);

        $('.msg_history').animate({ scrollTop: 20000000 }, "slow");
    }

}(app.message, app.common));