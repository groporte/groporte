﻿using Microsoft.AspNetCore.Identity;
using System;

namespace Groporte.Web.Models
{
    public class ApplicationUser : IdentityUser<Guid>
    {
    }
}
