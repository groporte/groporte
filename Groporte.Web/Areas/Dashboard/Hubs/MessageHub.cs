﻿using Groporte.Core.Interfaces.Providers;
using Groporte.Core.Interfaces.Services;
using Groporte.Core.Models;
using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace Groporte.Web.Areas.Dashboard.Hubs
{
    public class MessageHub : Hub
    {
        private readonly IConversationService _conversationService;
        private readonly IUserContext _userContext;

        public MessageHub(
            IConversationService conversationService,
            IUserContext userContext)
        {
            _conversationService = conversationService;
            _userContext = userContext;
        }

        public async Task SendMessageAsync(Conversation conversation)
        {
            var savedConversation = await _conversationService.SendMessageAsync(conversation);

            await Clients.All.SendAsync("ReceiveMessage", savedConversation);
        }
    }
}
