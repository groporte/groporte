﻿using Groporte.Core.Interfaces.Services;
using Groporte.Core.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Groporte.Web.Areas.Dashboard.Controllers
{
    public class MessageController : BaseController
    {
        private readonly IConversationService _conversationService;

        public MessageController(IConversationService conversationService)
        {
            _conversationService = conversationService;
        }

        // GET: Message
        public async Task<ActionResult> IndexAsync(Guid? orderId = null)
        {
            var orderMessages = await _conversationService.GetOrderMessagesAsync();

            //var orderMessageHistory = await GetOrderMessageHistoryAsync(orderId ?? orderMessages.FirstOrDefault()?.OrderId);

            //var model = new KeyValuePair<List<OrderMessage>, List<Conversation>>(orderMessages, orderMessageHistory);

            return View(orderMessages);
        }

        [HttpPost]
        public async Task<Conversation> SendMessageAsync(Conversation conversation)
        {
            var savedMessage = await _conversationService.SendMessageAsync(conversation);

            return savedMessage;
        }

        public async Task<PartialViewResult> GetOrderMessageHistoryPartialAsync(Guid? orderId = null)
        {
            var model = await _conversationService.GetConversationsAsync(orderId);

            return PartialView("_MessageHistoryPartial", model);
        }

        [HttpPost]
        public async Task<bool> IsOutgoingAsync(Guid senderId)
        {
            return await _conversationService.IsOutgoingAsync(senderId);
        }

        private async Task<List<Conversation>> GetOrderMessageHistoryAsync(Guid? orderId = null)
        {
            return await _conversationService.GetConversationsAsync(orderId);
        }
    }
}