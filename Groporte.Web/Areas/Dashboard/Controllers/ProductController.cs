﻿using Groporte.Core.Helpers;
using Groporte.Core.Interfaces.Services;
using Groporte.Core.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Groporte.Web.Areas.Dashboard.Controllers
{
    public class ProductController : BaseController
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        // GET: Product
        public async Task<IActionResult> Index()
        {
            var model = await _productService.GetAllAsync();

            return View(model);
        }

        // GET: Product/Details/5
        public async Task<IActionResult> Details(Guid id)
        {
            var model = await _productService.GetAsync(id);

            return PartialView(model);
        }

        // GET: Product/Create
        public IActionResult Create()
        {
            var model = new Product();

            return PartialView(model);
        }

        // POST: Product/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Product product)
        {
            try
            {
                await _productService.SaveProductAsync(null, product);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                while (!ex.InnerException.IsNull()) ex = ex.InnerException;

                return BadRequest(ex.Message);
            }
        }

        // GET: Product/Edit/5
        public async Task<IActionResult> Edit(Guid id)
        {
            var model = await _productService.GetAsync(id);

            return PartialView(model);
        }

        // POST: Product/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, Product product)
        {
            try
            {
                await _productService.SaveProductAsync(id, product);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                while (!ex.InnerException.IsNull()) ex = ex.InnerException;

                return BadRequest(ex.Message);
            }
        }

        // POST: Product/Delete/5
        [HttpPost]
        public async Task<IActionResult> Delete(Guid id)
        {
            try
            {
                await _productService.DeleteAsync(id);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                while (!ex.InnerException.IsNull()) ex = ex.InnerException;

                return BadRequest(ex.Message);
            }
        }
    }
}