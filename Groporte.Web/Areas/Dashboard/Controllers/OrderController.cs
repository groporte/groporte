﻿using Groporte.Core.Helpers;
using Groporte.Core.Interfaces.Services;
using Groporte.Core.SearchCriteriaModels;
using Groporte.Core.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Groporte.Web.Areas.Dashboard.Controllers
{
    public class OrderController : BaseController
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        // GET: Order
        public async Task<IActionResult> Index(OrderSearchCriteriaModel searchCriteria)
        {
            var model = await _orderService.GetOrdersAsync(searchCriteria);

            return View(model);
        }

        // GET: Order/Details/5
        public async Task<IActionResult> Details(Guid id)
        {
            var model = await _orderService.GetOrderDetailAsync(id);

            return PartialView(model);
        }

        // GET: Order/Create
        public IActionResult Create()
        {
            var model = new SaveOrderModel();

            return PartialView(model);
        }

        // POST: Order/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(SaveOrderModel saveOrderModel)
        {
            try
            {
                await _orderService.SaveOrderAsync(null, saveOrderModel);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                while (!ex.InnerException.IsNull()) ex = ex.InnerException;

                return BadRequest(ex.Message);
            }
        }

        // GET: Order/Edit/5
        public async Task<IActionResult> Edit(Guid id)
        {
            var model = await _orderService.GetSaveOrderModelAsync(id);

            return PartialView(model);
        }

        // POST: Order/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid? id, SaveOrderModel saveOrderModel)
        {
            try
            {
                await _orderService.SaveOrderAsync(id, saveOrderModel);

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                while (!ex.InnerException.IsNull()) ex = ex.InnerException;

                return BadRequest(ex.Message);
            }
        }

        // GET: Order/Cancel/5
        public IActionResult Cancel(Guid? id)
        {
            if (!id.HasValue) return BadRequest("Id is null");

            return PartialView();
        }

        // POST Order/Cancel/5
        [HttpPost]
        public async Task<IActionResult> Cancel(Guid id)
        {
            try
            {
                await _orderService.CancelOrderAsync(id);

                return NoContent();
            }
            catch (Exception ex)
            {
                while (!ex.InnerException.IsNull()) ex = ex.InnerException;

                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult Pay(Guid id)
        {
            // todo: handle payment using escrow
            return NoContent();
        }
    }
}