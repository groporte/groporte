using Groporte.Core.Constants;
using Groporte.Web.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using System.Threading.Tasks;

public class SeedData
{
    public static async Task EnsureSeedData(IServiceProvider provider)
    {
        using (var roleManager = provider.GetRequiredService<RoleManager<ApplicationRole>>())
        {
            if (!roleManager.Roles.Any())
            {
                await roleManager.CreateAsync(new ApplicationRole
                {
                    Name = RolesConstant.Buyer
                });

                await roleManager.CreateAsync(new ApplicationRole
                {
                    Name = RolesConstant.Agent
                });
            }
        }
    }
}
