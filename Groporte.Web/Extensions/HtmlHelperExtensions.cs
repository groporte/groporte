﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Groporte.Web.Extensions
{
    public static class HtmlHelperExtensions
    {
        public static IHtmlContent JsonSerialize(this IHtmlHelper htmlHelper, object data, bool camelCasing = true)
        {
            if (!camelCasing) return htmlHelper.Raw(JsonConvert.SerializeObject(data));

            var settings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            return htmlHelper.Raw(JsonConvert.SerializeObject(data, settings));
        }
    }
}
