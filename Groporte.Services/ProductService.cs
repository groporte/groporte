﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Groporte.Core.Interfaces.Repositories;
using Groporte.Core.Interfaces.Services;
using Groporte.Core.Models;

namespace Groporte.Services
{
    internal class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public async Task DeleteAsync(Guid id)
        {
            await _productRepository.DeleteAsync(id);
        }

        public async Task<List<Product>> GetAllAsync()
        {
            return await _productRepository.GetAllAsync();
        }

        public async Task<Product> GetAsync(Guid id)
        {
            return await _productRepository.GetAsync(id);
        }

        public async Task SaveProductAsync(Guid? id, Product product)
        {
            if (id.HasValue)
            {
                await _productRepository.UpdateAsync(product);
            }
            else
            {
                await _productRepository.AddAsync(product);
            }
        }
    }
}
