﻿using Groporte.Core.Interfaces.Providers;
using Groporte.Core.Interfaces.Repositories;
using Groporte.Core.Interfaces.Services;
using Groporte.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Groporte.Services
{
    internal class ConversationService : IConversationService
    {
        private readonly IConversationRepository _conversationRepository;
        private readonly IUserContext _userContext;

        public ConversationService(
            IConversationRepository conversationRepository,
            IUserContext userContext)
        {
            _conversationRepository = conversationRepository;
            _userContext = userContext;
        }

        public async Task<List<Conversation>> GetConversationsAsync(Guid? orderId)
        {
            orderId = orderId ?? await GetLastOrderIdAsync();

            var conversations = await _conversationRepository.GetAsync(orderId: orderId);

            conversations.ForEach(async x => x.IsOutgoing = await IsOutgoingAsync(x.SenderId.GetValueOrDefault()));

            return conversations;
        }

        public async Task<List<OrderMessage>> GetOrderMessagesAsync()
        {
            var userId = _userContext.UserId();

            return await _conversationRepository.GetOrderMessages(await _userContext.IsUserIdAdminAsync(userId) ? new Guid?() : userId);
        }

        public async Task<Guid> GetLastOrderIdAsync()
        {
            var userId = _userContext.UserId();

            return await _conversationRepository.GetLastOrderIdAsync(await _userContext.IsUserIdAdminAsync(userId) ? new Guid?() : userId);
        }

        public async Task<Conversation> SendMessageAsync(Conversation conversation)
        {
            var userId = _userContext.UserId();

            conversation.CreatedById = userId;
            conversation.ModifiedById = userId;
            conversation.SenderId = userId;

            var savedConversation = await _conversationRepository.AddAsync(conversation);

            return savedConversation;
        }

        public async Task<bool> IsOutgoingAsync(Guid senderId)
        {
            var userId = _userContext.UserId();
            var isUserAdmin = _userContext.IsAdmin();

            return senderId == userId || (isUserAdmin && await _userContext.IsUserIdAdminAsync(senderId));
        }
    }
}
