﻿using Groporte.Core.Enums;
using Groporte.Core.Interfaces.Providers;
using Groporte.Core.Interfaces.Repositories;
using Groporte.Core.Interfaces.Services;
using Groporte.Core.Models;
using Groporte.Core.SearchCriteriaModels;
using Groporte.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Groporte.Services
{
    internal class OrderService : IOrderService
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IUserContext _userContext;

        public OrderService(
            IOrderRepository orderRepository,
            IUserContext userContext)
        {
            _orderRepository = orderRepository;
            _userContext = userContext;
        }

        public async Task CancelOrderAsync(Guid id)
        {
            await _orderRepository.UpdateOrderStatusAsync(id, OrderStatusEnum.Cancelled);
        }

        public async Task<Order> GetOrderDetailAsync(Guid id)
        {
            return await _orderRepository.GetAsync(id);
        }

        public async Task<List<Order>> GetOrdersAsync(OrderSearchCriteriaModel searchCriteria)
        {
            searchCriteria.UserId = _userContext.IsAdmin() ? new Guid?() : _userContext.UserId();

            return await _orderRepository.GetAllAsync(searchCriteria);
        }

        public async Task<SaveOrderModel> GetSaveOrderModelAsync(Guid? id)
        {
            return id.HasValue ? await _orderRepository.GetSaveOrderModelAsync(id.Value) : new SaveOrderModel();
        }

        public async Task SaveOrderAsync(Guid? id, SaveOrderModel saveOrderModel)
        {
            if (id.HasValue)
            {
                await _orderRepository.UpdateAsync(id.Value, saveOrderModel);
            }
            else
            {
                await _orderRepository.AddAsync(saveOrderModel);
            }
        }
    }
}
