﻿using Groporte.Core.Interfaces.Services;
using Microsoft.Extensions.DependencyInjection;

namespace Groporte.Services.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServiceDependencies(this IServiceCollection services)
        {
            services.AddScoped<IConversationService, ConversationService>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IProductService, ProductService>();
            
            return services;
        }
    }
}
