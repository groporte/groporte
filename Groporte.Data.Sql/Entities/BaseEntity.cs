﻿using System;

namespace Groporte.Data.Sql.Entities
{
    internal class BaseEntity
    {
        public Guid Id { get; set; }

        public DateTime Created { get; set; }

        public Guid CreatedById { get; set; }

        public virtual User CreatedBy { get; set; }

        public DateTime? Modified { get; set; }

        public Guid? ModifiedById { get; set; }

        public virtual User ModifiedBy { get; set; }
    }
}
