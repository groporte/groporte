﻿using System.Collections.Generic;

namespace Groporte.Data.Sql.Entities
{
    internal class Product : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public List<SupplierProductPrice> SupplierProductPrices { get; set; }
    }
}
