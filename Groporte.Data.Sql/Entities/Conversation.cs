﻿using Groporte.Core.Enums;
using System;

namespace Groporte.Data.Sql.Entities
{
    internal class Conversation : BaseEntity
    {
        public Guid OrderId { get; set; }

        public Guid? SenderId { get; set; }

        public string Message { get; set; }

        public MessageStatus Status { get; set; }

        public Order Order { get; set; }

        public User Sender { get; set; }
    }
}
