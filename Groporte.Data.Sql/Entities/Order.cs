﻿using Groporte.Core.Enums;
using System;
using System.Collections.Generic;

namespace Groporte.Data.Sql.Entities
{
    internal class Order : BaseEntity
    {
        public string OrderNumber { get; set; }

        public Guid ProductId { get; set; }

        public int NumberOfTonnes { get; set; }

        public string ProductRequirement { get; set; }

        public OrderStatusEnum OrderStatus { get; set; }

        public decimal Price { get; set; }

        public Product Product { get; set; }

        public List<Conversation> Conversations { get; set; } = new List<Conversation>();
    }
}
