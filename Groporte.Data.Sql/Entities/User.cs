﻿using System;

namespace Groporte.Data.Sql.Entities
{
    internal class User
    {
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Email { get; set; }

        public bool Active { get; set; }

        public DateTime Created { get; set; }

        public DateTime? Modified { get; set; }

        public Guid? ModifiedById { get; set; }

        public virtual User ModifiedBy { get; set; }
    }
}
