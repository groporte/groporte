﻿using System;

namespace Groporte.Data.Sql.Entities
{
    internal class SupplierProductPrice : BaseEntity
    {
        public Guid SupplierId { get; set; }

        public Supplier Supplier { get; set; }

        public Guid ProductId { get; set; }

        public Product Product { get; set; }

        public decimal PricePerTonne { get; set; }

        public bool InStock { get; set; } = true;
    }
}
