﻿using Groporte.Data.Sql.Entities;
using Groporte.Data.Sql.Mappings;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Groporte.Data.Sql.DbContexts
{
    internal class GroporteDbContext : DbContext
    {
        public GroporteDbContext(DbContextOptions<GroporteDbContext> options)
            : base(options)
        {
        }

        internal DbSet<Conversation> Conversations { get; set; }

        internal DbSet<Order> Orders { get; set; }

        internal DbSet<Product> Products { get; set; }

        internal DbSet<SupplierProductPrice> SupplierProductPrices { get; set; }

        internal DbSet<Supplier> Suppliers { get; set; }

        internal DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new ConversationMap());
            modelBuilder.ApplyConfiguration(new OrderMap());
            modelBuilder.ApplyConfiguration(new ProductMap());
            modelBuilder.ApplyConfiguration(new SupplierMap());
            modelBuilder.ApplyConfiguration(new SupplierProductMap());
            modelBuilder.ApplyConfiguration(new UserMap());
        }

        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            OnBeforeSaving();

            return base.SaveChanges(acceptAllChangesOnSuccess);
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = new CancellationToken())
        {
            OnBeforeSaving();

            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void OnBeforeSaving()
        {
            var entityEntries = ChangeTracker.Entries<BaseEntity>();

            foreach (var entityEntry in entityEntries)
            {
                var baseEntity = entityEntry.Entity;
                var now = DateTime.Now;

                switch (entityEntry.State)
                {
                    case EntityState.Modified:
                        baseEntity.Modified = now;
                        baseEntity.Created = entityEntry.OriginalValues.GetValue<DateTime>("Created");
                        baseEntity.CreatedById = entityEntry.OriginalValues.GetValue<Guid>("CreatedById");
                        break;

                    case EntityState.Added:
                        baseEntity.Created = now;
                        baseEntity.Modified = now;
                        break;
                }
            }
        }
    }
}
