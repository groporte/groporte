﻿using Groporte.Core.Interfaces.Repositories;
using Groporte.Core.Models;
using Groporte.Data.Sql.DbContexts;
using Groporte.Data.Sql.Extensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Groporte.Data.Sql.Repositories
{
    internal class ConversationRepository : IConversationRepository
    {
        private readonly GroporteDbContext _groporteDbContext;

        public ConversationRepository(GroporteDbContext groporteDbContext)
        {
            _groporteDbContext = groporteDbContext;
        }

        public async Task<Conversation> AddAsync(Conversation conversation)
        {
            var entity = conversation.ToEntity();

            _groporteDbContext.Conversations.Add(entity);

            await _groporteDbContext.SaveChangesAsync();

            return new Conversation
            {
                Id = entity.Id,
                Created = entity.Created,
                CreatedById = entity.CreatedById,
                Message = entity.Message,
                OrderId = entity.OrderId,
                SenderId = entity.SenderId,
                Status = entity.Status
            };
        }

        public async Task DeleteAsync(Guid id)
        {
            var entity = await _groporteDbContext.Conversations.FindAsync(id);

            _groporteDbContext.Conversations.Remove(entity);

            await _groporteDbContext.SaveChangesAsync();
        }

        public async Task<List<Conversation>> GetAsync(Guid? orderId = null, Guid? userId = null)
        {
            var query = _groporteDbContext.Conversations.AsQueryable();

            if (orderId.HasValue)
            {
                query = query.Where(x => x.OrderId == orderId);
            }

            if (userId.HasValue)
            {
                query = query.Where(x =>
                    x.CreatedById == userId
                    || x.SenderId == userId
                    || x.Order.CreatedById == userId
                    || x.Order.ModifiedById == userId);
            }

            return await query.Select(x => new Conversation
            {
                Id = x.Id,
                Created = x.Created,
                CreatedById = x.CreatedById,
                Message = x.Message,
                OrderId = x.OrderId,
                SenderId = x.SenderId,
                Status = x.Status
            }).OrderByDescending(x => x.OrderId).ThenBy(x => x.Created).ToListAsync();
        }

        public Task<List<OrderMessage>> GetOrderMessages(Guid? userId = null)
        {
            if (userId.HasValue)
            {
                return _groporteDbContext.Orders.Where(x => x.CreatedById == userId || x.ModifiedById == userId).OrderByDescending(x => x.OrderNumber).Select(x => new OrderMessage
                {
                    OrderId = x.Id,
                    OrderNumber = x.OrderNumber,
                    Message = x.Conversations.OrderByDescending(y => y.Created).Select(y => y.Message).FirstOrDefault() ?? x.ProductRequirement,
                    Date = x.Conversations.Any() ? x.Conversations.OrderByDescending(y => y.Created).Select(y => y.Created).FirstOrDefault() : x.Modified
                }).ToListAsync();
            }

            return _groporteDbContext.Orders.OrderByDescending(x => x.OrderNumber).Select(x => new OrderMessage
            {
                OrderId = x.Id,
                OrderNumber = x.OrderNumber,
                Message = x.Conversations.OrderByDescending(y => y.Created).Select(y => y.Message).FirstOrDefault() ?? x.ProductRequirement,
                Date = x.Conversations.Any() ? x.Conversations.OrderByDescending(y => y.Created).Select(y => y.Created).FirstOrDefault() : x.Modified
            }).ToListAsync();
        }

        public async Task<Guid> GetLastOrderIdAsync(Guid? userId = null)
        {
            return await _groporteDbContext.Orders.Where(x => userId.HasValue ? x.CreatedById == userId || x.ModifiedById == userId : true).OrderByDescending(x => x.OrderNumber).Select(x => x.Id).FirstOrDefaultAsync();
        }

        public async Task UpdateAsync(Conversation conversation)
        {
            var entity = await _groporteDbContext.Conversations.FindAsync(conversation.Id);

            entity.Status = conversation.Status;

            await _groporteDbContext.SaveChangesAsync();
        }
    }
}
