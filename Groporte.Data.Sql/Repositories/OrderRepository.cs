﻿using Groporte.Core.Enums;
using Groporte.Core.Helpers;
using Groporte.Core.Interfaces.Providers;
using Groporte.Core.Interfaces.Repositories;
using Groporte.Core.Models;
using Groporte.Core.SearchCriteriaModels;
using Groporte.Core.ViewModels;
using Groporte.Data.Sql.DbContexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Groporte.Data.Sql.Repositories
{
    internal class OrderRepository : IOrderRepository
    {
        public readonly GroporteDbContext _groporteDbContext;
        private readonly IUserContext _userContext;

        public OrderRepository(GroporteDbContext groporteDbContext, IUserContext userContext)
        {
            _groporteDbContext = groporteDbContext;
            _userContext = userContext;
        }

        public async Task<Guid> AddAsync(SaveOrderModel model)
        {
            var userId = _userContext.UserId();

            var entity = new Entities.Order
            {
                NumberOfTonnes = model.NumberOfTonnes,
                ProductId = model.ProductId.GetValueOrDefault(),
                ProductRequirement = model.ProductRequirement,
                OrderStatus = OrderStatusEnum.Pending,
                OrderNumber = $"{await _groporteDbContext.Orders.CountAsync().ContinueWith(x => x.Result + 1)}",
                CreatedById = userId,
                ModifiedById = userId
            };

            _groporteDbContext.Orders.Add(entity);

            await _groporteDbContext.SaveChangesAsync();

            return entity.Id;
        }

        public async Task DeleteAsync(Guid id)
        {
            var entity = await _groporteDbContext.Orders.FindAsync(id);

            _groporteDbContext.Orders.Remove(entity);

            await _groporteDbContext.SaveChangesAsync();
        }

        public async Task<List<Order>> GetAllAsync(OrderSearchCriteriaModel searchCriteria)
        {
            var searchQuery = _groporteDbContext.Orders.Include(x => x.Product).AsNoTracking();

            if (!string.IsNullOrEmpty(searchCriteria.Product))
            {
                searchQuery = searchQuery.Where(x => x.Product.Name.Contains(searchCriteria.Product));
            }

            if (searchCriteria.MaximumPrice.HasValue)
            {
                searchQuery = searchQuery.Where(x => x.Price <= searchCriteria.MaximumPrice);
            }

            if (searchCriteria.MinimumPrice.HasValue)
            {
                searchQuery = searchQuery.Where(x => x.Price >= searchCriteria.MinimumPrice);
            }

            if (searchCriteria.CreatedFrom.HasValue)
            {
                searchQuery = searchQuery.Where(x => x.Created >= searchCriteria.CreatedFrom);
            }

            if (searchCriteria.CreatedTo.HasValue)
            {
                searchQuery = searchQuery.Where(x => x.Created <= searchCriteria.CreatedTo);
            }

            if (searchCriteria.QuantityFrom.HasValue)
            {
                searchQuery = searchQuery.Where(x => x.NumberOfTonnes >= searchCriteria.QuantityFrom);
            }

            if (searchCriteria.QuantityTo.HasValue)
            {
                searchQuery = searchQuery.Where(x => x.NumberOfTonnes <= searchCriteria.QuantityTo);
            }

            if (searchCriteria.Status.HasValue)
            {
                searchQuery = searchQuery.Where(x => x.OrderStatus == searchCriteria.Status);
            }

            if (searchCriteria.UserId.HasValue)
            {
                searchQuery = searchQuery.Where(x => x.CreatedById == searchCriteria.UserId);
            }

            return await searchQuery.Select(x => new Order
            {
                Id = x.Id,
                Created = x.Created,
                Product = new NamedId(x.ProductId, x.Product.Name),
                ProductId = x.ProductId,
                NumberOfTonnes = x.NumberOfTonnes,
                Price = x.Price,
                OrderNumber = x.OrderNumber,
                OrderStatus = x.OrderStatus
            }).OrderByDescending(x => x.Created).ToListAsync();
        }

        public async Task<Order> GetAsync(Guid id)
        {
            var order = await _groporteDbContext.Orders.Where(x => x.Id == id).Select(x => new Order
            {
                Id = x.Id,
                NumberOfTonnes = x.NumberOfTonnes,
                OrderNumber = x.OrderNumber,
                OrderStatus = x.OrderStatus,
                Price = x.Price,
                ProductId = x.ProductId,
                Product = new NamedId(x.ProductId, x.Product.Name),
                ProductRequirement = x.ProductRequirement
            }).FirstOrDefaultAsync();

            return order;
        }

        public async Task<SaveOrderModel> GetSaveOrderModelAsync(Guid id)
        {
            return await _groporteDbContext.Orders
                .Where(x => x.Id == id)
                .Include(x => x.Product)
                .Select(x => new SaveOrderModel
                {
                    NumberOfTonnes = x.NumberOfTonnes,
                    OrderStatus = x.OrderStatus,
                    ProductId = x.ProductId,
                    ProductRequirement = x.ProductRequirement
                }).FirstOrDefaultAsync();
        }

        public async Task UpdateAsync(Guid id, SaveOrderModel model)
        {
            var entity = await _groporteDbContext.Orders.FindAsync(id);

            entity.NumberOfTonnes = model.NumberOfTonnes;
            entity.ProductId = model.ProductId.GetValueOrDefault();
            entity.ProductRequirement = model.ProductRequirement;

            if (model.OrderStatus.HasValue && model.OrderStatus != OrderStatusEnum.None)
            {
                entity.OrderStatus = model.OrderStatus.GetValueOrDefault();
            }

            entity.ModifiedById = _userContext.UserId();

            if (_groporteDbContext.Entry(entity).State == EntityState.Detached)
            {
                _groporteDbContext.Orders.Attach(entity);
            }

            await _groporteDbContext.SaveChangesAsync();
        }

        public async Task UpdateOrderStatusAsync(Guid id, OrderStatusEnum orderStatus)
        {
            var entity = await _groporteDbContext.Orders.FindAsync(id);

            if (entity.OrderStatus == OrderStatusEnum.InProgress || entity.OrderStatus == OrderStatusEnum.Completed)
            {
                throw new Exception("Order can not be cancelled as it is either in transit or completed");
            }

            entity.OrderStatus = orderStatus;

            await _groporteDbContext.SaveChangesAsync();
        }
    }
}
