﻿using Groporte.Core.Interfaces.Repositories;
using Groporte.Core.Models;
using Groporte.Data.Sql.DbContexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Groporte.Data.Sql.Repositories
{
    internal class UserRepository : IUserRepository
    {
        private readonly GroporteDbContext _groporteDbContext;

        public UserRepository(GroporteDbContext groporteDbContext)
        {
            _groporteDbContext = groporteDbContext;
        }

        public async Task<Guid> AddAsync(User user)
        {
            var entity = new Entities.User
            {
                Id = user.Id,
                Active = user.Active,
                Email = user.Email,
                FirstName = user.Email,
                LastName = user.LastName,
                Created = DateTime.Now
            };

            _groporteDbContext.Users.Add(entity);

            await _groporteDbContext.SaveChangesAsync();

            return entity.Id;
        }

        public async Task<User> GetUserAsync(Guid id)
        {
            return await _groporteDbContext.Users.Where(x => x.Id == id).Select(x => new User
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Email = x.Email
            }).FirstOrDefaultAsync();
        }

        public async Task UpdateAsync(User user)
        {
            var dbUser = await _groporteDbContext.Users.FindAsync(user.Id);

            if (dbUser == null)
            {
                throw new NullReferenceException($"No user found with id {user.Id}");
            }

            dbUser.FirstName = user.FirstName;
            dbUser.LastName = user.LastName;
            dbUser.Email = user.Email;

            await _groporteDbContext.SaveChangesAsync();
        }
    }
}
