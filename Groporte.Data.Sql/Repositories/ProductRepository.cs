﻿using Groporte.Core.Helpers;
using Groporte.Core.Interfaces.Providers;
using Groporte.Core.Interfaces.Repositories;
using Groporte.Core.Models;
using Groporte.Data.Sql.DbContexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Groporte.Data.Sql.Repositories
{
    internal class ProductRepository : IProductRepository
    {
        private readonly GroporteDbContext _groporteDbContext;
        private readonly IUserContext _userContext;

        public ProductRepository(GroporteDbContext groporteDbContext, IUserContext userContext)
        {
            _groporteDbContext = groporteDbContext;
            _userContext = userContext;
        }

        public async Task<List<Product>> GetAllAsync()
        {
            return await _groporteDbContext.Products.Select(x => new Product
            {
                Id = x.Id,
                Created = x.Created,
                CreatedById = x.CreatedById,
                Description = x.Description,
                Modified = x.Modified,
                ModifiedById = x.ModifiedById,
                Name = x.Name
            }).ToListAsync();
        }

        public async Task<Product> GetAsync(Guid id)
        {
            return await _groporteDbContext.Products.Where(x => x.Id == id).Select(x => new Product
            {
                Id = x.Id,
                Created = x.Created,
                CreatedById = x.CreatedById,
                Description = x.Description,
                Modified = x.Modified,
                ModifiedById = x.ModifiedById,
                Name = x.Name
            }).FirstOrDefaultAsync();
        }

        public async Task<Guid> AddAsync(Product product)
        {
            var entity = new Entities.Product
            {
                Name = product.Name,
                Description = product.Description,
                CreatedById = _userContext.UserId(),
                ModifiedById = _userContext.UserId()
            };

            _groporteDbContext.Products.Add(entity);

            await _groporteDbContext.SaveChangesAsync();

            return entity.Id;
        }

        public async Task DeleteAsync(Guid id)
        {
            var entity = await _groporteDbContext.Products.FindAsync(id);

            if (entity.IsNull()) return;

            _groporteDbContext.Products.Remove(entity);

            await _groporteDbContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(Product product)
        {
            var entity = await _groporteDbContext.Products.FindAsync(product.Id);

            if (entity == null)
            {
                throw new NullReferenceException($"Product with id {product.Id} does not exist");
            }

            entity.Name = product.Name;
            entity.Description = product.Description;
            entity.ModifiedById = _userContext.UserId();

            _groporteDbContext.Products.Update(entity);

            await _groporteDbContext.SaveChangesAsync();
        }
    }
}
