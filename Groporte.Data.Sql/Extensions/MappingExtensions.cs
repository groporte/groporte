﻿using Groporte.Core.Models;

namespace Groporte.Data.Sql.Extensions
{
    internal static class MappingExtensions
    {
        internal static Entities.Conversation ToEntity(this Conversation conversation)
        {
            return new Entities.Conversation
            {
                Id = conversation.Id,
                CreatedById = conversation.CreatedById,
                ModifiedById = conversation.ModifiedById,
                Message = conversation.Message,
                OrderId = conversation.OrderId,
                //ReceiverId = conversation.ReceiverId,
                SenderId = conversation.SenderId,
                Status = conversation.Status
            };
        }
    }
}
