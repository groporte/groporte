﻿using Groporte.Data.Sql.Entities;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Groporte.Data.Sql.Extensions
{
    internal static class EntityTypeBuilderExtensions
    {
        public static void ConfigureBaseEntity<T>(this EntityTypeBuilder<T> builder) where T : BaseEntity
        {
            builder.Property(x => x.Created).ValueGeneratedOnAdd();
            builder.Property(x => x.Modified).ValueGeneratedOnUpdate();

            builder.HasOne(x => x.CreatedBy).WithMany().HasForeignKey(x => x.CreatedById).IsRequired();
            builder.HasOne(x => x.ModifiedBy).WithMany().HasForeignKey(x => x.ModifiedById);
        }
    }
}
