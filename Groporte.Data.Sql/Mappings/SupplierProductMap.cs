﻿using Groporte.Data.Sql.Entities;
using Groporte.Data.Sql.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Groporte.Data.Sql.Mappings
{
    internal class SupplierProductMap : IEntityTypeConfiguration<SupplierProductPrice>
    {
        public void Configure(EntityTypeBuilder<SupplierProductPrice> builder)
        {
            builder.ConfigureBaseEntity();

            builder.HasOne(x => x.Supplier).WithMany(x => x.SupplierProductPrices).HasForeignKey(x => x.SupplierId).IsRequired().OnDelete(DeleteBehavior.NoAction);
            builder.HasOne(x => x.Product).WithMany(x => x.SupplierProductPrices).HasForeignKey(x => x.ProductId).IsRequired().OnDelete(DeleteBehavior.NoAction);
        }
    }
}
