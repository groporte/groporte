﻿using Groporte.Data.Sql.Entities;
using Groporte.Data.Sql.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Groporte.Data.Sql.Mappings
{
    internal class ConversationMap : IEntityTypeConfiguration<Conversation>
    {
        public void Configure(EntityTypeBuilder<Conversation> builder)
        {
            builder.ConfigureBaseEntity();

            builder.HasOne(x => x.Sender).WithMany().HasForeignKey(x => x.SenderId);

            //builder.HasOne(x => x.Receiver).WithMany().HasForeignKey(x => x.ReceiverId);

            builder.HasOne(x => x.Order).WithMany(x => x.Conversations).HasForeignKey(x => x.OrderId).OnDelete(DeleteBehavior.Restrict);
        }
    }
}
