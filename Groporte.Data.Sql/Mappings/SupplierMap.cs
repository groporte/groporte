﻿using Groporte.Data.Sql.Entities;
using Groporte.Data.Sql.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Groporte.Data.Sql.Mappings
{
    internal class SupplierMap : IEntityTypeConfiguration<Supplier>
    {
        public void Configure(EntityTypeBuilder<Supplier> builder)
        {
            builder.ConfigureBaseEntity();
        }
    }
}
