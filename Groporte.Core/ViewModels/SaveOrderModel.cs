﻿using Groporte.Core.Enums;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Groporte.Core.ViewModels
{
    public class SaveOrderModel
    {
        [Required]
        public Guid? ProductId { get; set; }

        [DisplayName("Number of Tonnes")]
        [Required, Range(1, int.MaxValue)]
        public int NumberOfTonnes { get; set; }

        [DisplayName("Product Requirement")]
        [Required(AllowEmptyStrings = false)]
        public string ProductRequirement { get; set; }

        public OrderStatusEnum? OrderStatus { get; set; }
    }
}
