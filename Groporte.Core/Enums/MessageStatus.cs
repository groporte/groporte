﻿namespace Groporte.Core.Enums
{
    public enum MessageStatus
    {
        None = 0,
        Sent = 10,
        Delivered = 20,
        Read = 30
    }
}
