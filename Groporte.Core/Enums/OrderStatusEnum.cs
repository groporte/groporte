﻿using System.ComponentModel;

namespace Groporte.Core.Enums
{
    public enum OrderStatusEnum
    {
        None = 0,

        [Description("Pending")]
        Pending = 100,

        [Description("In Progress")]
        InProgress = 200,

        [Description("Completed")]
        Completed = 300,

        [Description("Cancelled")]
        Cancelled = 400
    }
}
