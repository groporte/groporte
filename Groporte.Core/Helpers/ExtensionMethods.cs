﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace Groporte.Core.Helpers
{
    public static class ExtensionMethods
    {
        public static bool IsNull<T>(this T source)
        {
            return source == null;
        }

        public static bool IsNullOrEmpty<T>(this IEnumerable<T> source)
        {
            return source == null || !source.Any();
        }

        public static void ForEach<T>(this IEnumerable<T> source, Action<T> action)
        {
            if (source.IsNullOrEmpty()) return;

            foreach (var item in source)
            {
                action(item);
            }
        }

        public static string ToJson<T>(this T data, bool camelCasing = false)
        {
            if (!camelCasing) return JsonConvert.SerializeObject(data);

            var settings = new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            return JsonConvert.SerializeObject(data, settings);
        }

        public static T FromJson<T>(this string source)
        {
            return JsonConvert.DeserializeObject<T>(source);
        }

        public static bool IsAnyOf<T>(this T source, params T[] values)
        {
            if (values.IsNullOrEmpty()) return false;

            return values.Contains(source);
        }

        public static bool IsTrue(this string source)
        {
            if (source.IsNullOrEmpty()) return false;

            return (source.ToLower() == "true");
        }

        public static void MergeShallow<T, S>(this T target, S source, params string[] ignoreProperties)
        {
            if (target.IsNull() || source.IsNull())
            {
                return;
            }

            var type = typeof(S);
            var props = type.GetProperties();

            foreach (var prop in props)
            {

                if (!prop.CanWrite
                    || prop.PropertyType.IsArray
                    || prop.PropertyType.IsInterface
                    || (ignoreProperties != null && ignoreProperties.Contains(prop.Name))
                    || (prop.PropertyType.IsClass && prop.PropertyType.Name != "String")) // TODO: this is meant to exlude only the custom classes 
                {
                    continue;
                }

                var value = prop.GetValue(source);
                prop.SetValue(target, value);
            }
        }

        public static T ToEnum<T>(this string value) where T : struct
        {
            if (!Enum.TryParse<T>(value, out var result)) throw new ArgumentException("value is an invalid enum type");

            return result;
        }

        public static T DeepClone<T>(this T source)
        {
            var json = source.ToJson();

            return json.FromJson<T>();
        }

        public static string GetDescription<T>(this T @enum) where T : Enum
        {
            if (@enum.IsNull()) return string.Empty;

            var field = @enum.GetType().GetField(@enum.ToString());

            if (field.IsNull()) return string.Empty;

            return !(Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) is DescriptionAttribute attribute) ? @enum.ToString() : attribute.Description;
        }

        public static int ToInt(this string str)
        {
            int.TryParse(str, out var result);

            return result;
        }
    }
}
