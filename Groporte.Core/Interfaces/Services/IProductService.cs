﻿using Groporte.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Groporte.Core.Interfaces.Services
{
    public interface IProductService
    {
        Task SaveProductAsync(Guid? id, Product product);

        Task<Product> GetAsync(Guid id);

        Task<List<Product>> GetAllAsync();

        Task DeleteAsync(Guid id);
    }
}
