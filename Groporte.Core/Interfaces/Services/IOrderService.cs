﻿using Groporte.Core.Models;
using Groporte.Core.SearchCriteriaModels;
using Groporte.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Groporte.Core.Interfaces.Services
{
    public interface IOrderService
    {
        Task<List<Order>> GetOrdersAsync(OrderSearchCriteriaModel searchCriteria);

        Task<Order> GetOrderDetailAsync(Guid id);

        Task<SaveOrderModel> GetSaveOrderModelAsync(Guid? id);

        Task SaveOrderAsync(Guid? id, SaveOrderModel saveOrderModel);

        Task CancelOrderAsync(Guid id);
    }
}
