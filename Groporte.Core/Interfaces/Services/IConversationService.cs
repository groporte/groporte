﻿using Groporte.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Groporte.Core.Interfaces.Services
{
    public interface IConversationService
    {
        Task<List<Conversation>> GetConversationsAsync(Guid? orderId);

        Task<List<OrderMessage>> GetOrderMessagesAsync();

        Task<bool> IsOutgoingAsync(Guid senderId);

        Task<Conversation> SendMessageAsync(Conversation conversation);
    }
}
