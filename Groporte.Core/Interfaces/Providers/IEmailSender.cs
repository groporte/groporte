﻿using System.Threading.Tasks;

namespace Groporte.Core.Interfaces.Providers
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string htmlMessage);
    }
}
