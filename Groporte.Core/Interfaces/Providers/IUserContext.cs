﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Groporte.Core.Interfaces.Providers
{
    public interface IUserContext
    {
        Guid UserId();

        string GetUserEmail();

        IEnumerable<string> GetUserRoles();

        bool IsAdmin();

        Task<bool> IsUserIdAdminAsync(Guid id);
    }
}
