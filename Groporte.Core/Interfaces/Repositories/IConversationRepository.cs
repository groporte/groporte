﻿using Groporte.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Groporte.Core.Interfaces.Repositories
{
    public interface IConversationRepository
    {
        Task<List<Conversation>> GetAsync(Guid? orderId = null, Guid? userId = null);

        Task<List<OrderMessage>> GetOrderMessages(Guid? userId = null);

        Task<Guid> GetLastOrderIdAsync(Guid? userId = null);

        Task<Conversation> AddAsync(Conversation conversation);

        Task UpdateAsync(Conversation conversation);

        Task DeleteAsync(Guid id);
    }
}
