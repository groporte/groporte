﻿using Groporte.Core.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Groporte.Core.Interfaces.Repositories
{
    public interface IProductRepository
    {
        Task<List<Product>> GetAllAsync();

        Task<Product> GetAsync(Guid id);

        Task<Guid> AddAsync(Product product);

        Task UpdateAsync(Product product);

        Task DeleteAsync(Guid id);
    }
}
