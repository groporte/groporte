﻿using Groporte.Core.Enums;
using Groporte.Core.Models;
using Groporte.Core.SearchCriteriaModels;
using Groporte.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Groporte.Core.Interfaces.Repositories
{
    public interface IOrderRepository
    {
        Task<Guid> AddAsync(SaveOrderModel model);

        Task DeleteAsync(Guid id);

        Task<List<Order>> GetAllAsync(OrderSearchCriteriaModel searchCriteria);

        Task<Order> GetAsync(Guid id);

        Task<SaveOrderModel> GetSaveOrderModelAsync(Guid id);

        Task UpdateAsync(Guid id, SaveOrderModel model);

        Task UpdateOrderStatusAsync(Guid id, OrderStatusEnum orderStatus);
    }
}
