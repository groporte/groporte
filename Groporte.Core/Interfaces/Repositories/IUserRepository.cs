﻿using Groporte.Core.Models;
using System;
using System.Threading.Tasks;

namespace Groporte.Core.Interfaces.Repositories
{
    public interface IUserRepository
    {
        Task<Guid> AddAsync(User user);

        Task<User> GetUserAsync(Guid id);

        Task UpdateAsync(User user);
    }
}
