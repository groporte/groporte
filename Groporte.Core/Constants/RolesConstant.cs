namespace Groporte.Core.Constants
{
    public class RolesConstant
    {
        public const string Agent = "Agent";

        public const string Buyer = "Buyer";
    }
}