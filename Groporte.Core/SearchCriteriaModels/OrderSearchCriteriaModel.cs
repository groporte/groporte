﻿using Groporte.Core.Enums;
using System;

namespace Groporte.Core.SearchCriteriaModels
{
    public class OrderSearchCriteriaModel
    {
        public Guid? UserId { get; set; }

        public OrderStatusEnum? Status { get; set; }

        public DateTime? CreatedFrom { get; set; }

        public DateTime? CreatedTo { get; set; }

        public int? MinimumPrice { get; set; }

        public int? MaximumPrice { get; set; }

        public int? QuantityFrom { get; set; }

        public int? QuantityTo { get; set; }

        public string Product { get; set; }
    }
}
