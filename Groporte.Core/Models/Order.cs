﻿using Groporte.Core.Enums;
using System;
using System.ComponentModel.DataAnnotations;

namespace Groporte.Core.Models
{
    public class Order : BaseModel
    {
        [Display(Name = "Order Number")]
        public string OrderNumber { get; set; }

        public Guid ProductId { get; set; }

        public NamedId Product { get; set; }

        [Display(Name = "Number of Tonnes needed")]
        public double NumberOfTonnes { get; set; }

        [Display(Name = "Product Requirement")]
        public string ProductRequirement { get; set; }

        [Display(Name = "Status")]
        public OrderStatusEnum OrderStatus { get; set; }

        public decimal Price { get; set; }
    }
}
