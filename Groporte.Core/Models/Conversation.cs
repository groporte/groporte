﻿using Groporte.Core.Enums;
using System;

namespace Groporte.Core.Models
{
    public class Conversation : BaseModel
    {
        public string CreatedDate { get { return $"{Created.ToShortTimeString()}    |    {Created.ToString("MMM dd")}"; } }

        public Guid? SenderId { get; set; }

        public Guid OrderId { get; set; }

        public string Message { get; set; }

        public MessageStatus Status { get; set; }

        public NamedId Sender { get; set; }

        public bool IsOutgoing { get; set; }
    }
}
