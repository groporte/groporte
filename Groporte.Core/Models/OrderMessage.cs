﻿using System;

namespace Groporte.Core.Models
{
    public class OrderMessage
    {
        public string OrderNumber { get; set; }

        public Guid OrderId { get; set; }

        public string Message { get; set; }

        public DateTime? Date { get; set; }
    }
}
