﻿using System;

namespace Groporte.Core.Models
{
    public class SupplierProductPrice : BaseModel
    {
        public Guid SupplierId { get; set; }

        public Guid ProductId { get; set; }

        public decimal PricePerTonne { get; set; }

        public bool InStock { get; set; } = true;
    }
}
