﻿using System;

namespace Groporte.Core.Models
{
    public class NamedId
    {
        public NamedId()
        {
        }

        public NamedId(Guid id)
        {
            Id = id;
        }

        public NamedId(Guid id, string name)
        {
            Id = id;
            Name = name;
        }

        public Guid Id { get; set; }

        public string Name { get; set; }
    }
}
