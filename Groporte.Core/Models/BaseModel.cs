﻿using System;

namespace Groporte.Core.Models
{
    public class BaseModel
    {
        public Guid Id { get; set; }

        public DateTime Created { get; set; }

        public Guid CreatedById { get; set; }

        public DateTime? Modified { get; set; }

        public Guid? ModifiedById { get; set; }
    }
}
