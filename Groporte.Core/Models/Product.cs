﻿namespace Groporte.Core.Models
{
    public class Product : BaseModel
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
